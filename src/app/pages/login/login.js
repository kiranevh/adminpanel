﻿(function () {
    'use strict';

    angular
        .module('myTrends')
        .controller('LoginController', LoginController);

    
    function LoginController($location, AuthenticationService, FlashService) {
        
        var vm = this;

        vm.login = login;

        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success=='true') {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/dashboard');
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();
